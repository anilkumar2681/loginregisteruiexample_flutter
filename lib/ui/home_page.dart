
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget{
  HomePage({Key key}):super(key:key);

  @override
  _HomePageState createState()=> new _HomePageState();

}

class _HomePageState extends State<HomePage> {

  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
      ),
      body: Center(
        child: Text("Well Come To Home",
        style: style.copyWith(color: Colors.black,fontSize: 30.0,fontWeight: FontWeight.bold)),
      ),
    );
  }
}